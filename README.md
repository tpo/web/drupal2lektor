# Drupal2Lektor

A Python3 script to export data from Drupal 8+ using RESTful Web Services and
import it into a Lektor static site generator.

Node content, taxonomy terms (tags) and comments are supported.

The script is tailored for the migration project of
[blog.torproject.org](https://blog.torproject.org) but could be useful in other
contexts with some tweaking.

## Prerequisites

 * Drupal 8+
 * `apt install python3-requests python3-slugify`
 * `pip3 install markdownify`

### Drupal

To prepare the migration from Drupal to Lektor, some modifications to the Drupal
site must be made:

 1. Make sure the Views module is enabled
 1. Enable the RESTful Web Services module and dependencies
 2. Import the YAML configurations from the `drupal-configs` subdirectory

### Lektor

The Lektor site requires a model similar to the one below:

```
[model]
name = Blog Post
label = {{ this.title }}

[fields.title]
label = Title
type = string
size = large

[fields.author]
label = Author
type = string
width = 1/2

[fields.pub_date]
label = Publication date
type = date
width = 1/4

[fields.image]
label = Image
type = string

[fields.summary]
label = Summary
type = markdown

[fields.body]
label = Body
type = markdown

[fields.tags]
type = strings

[fields._html_body]
label = Legacy HMTL Body
type = html

[fields._comments]
label = Legacy comments
type = html
```

The `_html_body` and `_comments` fields are special-purposed to host HTML data
exported from Drupal content nodes and comments.

If a `summary` is available for the node body, it will be converted to markdown
from HTML.

## Running

An example command to trigger the import process:

```
./drupal2lektor --drupal-basic-auth basicuser:pass --drupal-user drupaluser --drupal-pass foo --drupal-url https://example.net --drupal-content-type article --lektor-base-path /my/lektor/site --lektor-image-path static/images/blog --lektor-model blog --overwrite --verbose
```
